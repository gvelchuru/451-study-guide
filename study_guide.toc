\contentsline {section}{\numberline {1}The Kernel Abstraction}{2}% 
\contentsline {subsection}{\numberline {1.1}Booting}{2}% 
\contentsline {subsection}{\numberline {1.2}Device Interrupts}{2}% 
\contentsline {subsection}{\numberline {1.3}Process Abstraction}{2}% 
\contentsline {subsection}{\numberline {1.4}Dual-Mode operation}{2}% 
\contentsline {subsection}{\numberline {1.5}Memory protection}{2}% 
\contentsline {subsubsection}{\numberline {1.5.1}Base + bound registers}{2}% 
\contentsline {subsubsection}{\numberline {1.5.2}Virtual addesses}{3}% 
\contentsline {subsubsection}{\numberline {1.5.3}Kalloc vs malloc}{3}% 
\contentsline {subsubsection}{\numberline {1.5.4}Hardware timer}{3}% 
\contentsline {subsubsection}{\numberline {1.5.5}Mode switching}{3}% 
\contentsline {subsection}{\numberline {1.6}Interrupts}{3}% 
\contentsline {subsection}{\numberline {1.7}Upcalls}{4}% 
\contentsline {subsection}{\numberline {1.8}Syscalls}{4}% 
\contentsline {subsubsection}{\numberline {1.8.1}Kernel System Call handler}{4}% 
\contentsline {subsection}{\numberline {1.9}User-level VM}{4}% 
\contentsline {subsection}{\numberline {1.10}Avoiding boundary crossings}{4}% 
\contentsline {section}{\numberline {2}The Programming Interface}{4}% 
\contentsline {subsection}{\numberline {2.1}Windows CreateProcess}{4}% 
\contentsline {subsection}{\numberline {2.2}Unix fork/exec}{5}% 
\contentsline {subsection}{\numberline {2.3}Unix I/O}{5}% 
\contentsline {subsubsection}{\numberline {2.3.1}UNIX File System Interface}{5}% 
\contentsline {subsection}{\numberline {2.4}Implementing a Shell}{5}% 
\contentsline {subsection}{\numberline {2.5}Shell Input/Output Redirection}{5}% 
\contentsline {section}{\numberline {3}Concurrency}{5}% 
\contentsline {subsection}{\numberline {3.1}Definitions}{5}% 
\contentsline {subsubsection}{\numberline {3.1.1}Is a kernel interrupt a thread?}{5}% 
\contentsline {subsection}{\numberline {3.2}Threads in the Kernel vs User-level}{5}% 
\contentsline {subsection}{\numberline {3.3}Thread operations}{6}% 
\contentsline {subsubsection}{\numberline {3.3.1}Other per-thread state}{6}% 
\contentsline {subsection}{\numberline {3.4}Thread lifecyle}{6}% 
\contentsline {subsection}{\numberline {3.5}Fork/Join Concurrency}{6}% 
\contentsline {subsection}{\numberline {3.6}Thread data structures}{6}% 
\contentsline {subsection}{\numberline {3.7}Implementing threads}{6}% 
\contentsline {subsection}{\numberline {3.8}Thread context switch}{6}% 
\contentsline {subsubsection}{\numberline {3.8.1}Context switch subtlety}{6}% 
\contentsline {subsection}{\numberline {3.9}Faster Thread/Process switch}{6}% 
\contentsline {subsection}{\numberline {3.10}Preemptive multi-threading}{6}% 
\contentsline {subsection}{\numberline {3.11}Schedule activations (modern OS)}{7}% 
\contentsline {subsection}{\numberline {3.12}Scheduling in xk}{7}% 
\contentsline {section}{\numberline {4}File Descriptors}{7}% 
\contentsline {subsection}{\numberline {4.1}Open}{7}% 
\contentsline {subsection}{\numberline {4.2}Close}{7}% 
\contentsline {subsection}{\numberline {4.3}Dup}{7}% 
\contentsline {subsection}{\numberline {4.4}Read/Write}{7}% 
\contentsline {subsection}{\numberline {4.5}Stat}{7}% 
\contentsline {section}{\numberline {5}Synchronization}{7}% 
\contentsline {subsection}{\numberline {5.1}Definitions}{7}% 
\contentsline {subsection}{\numberline {5.2}Too Much Milk}{8}% 
\contentsline {subsubsection}{\numberline {5.2.1}Solution 1}{8}% 
\contentsline {subsubsection}{\numberline {5.2.2}Solution 2}{8}% 
\contentsline {subsubsection}{\numberline {5.2.3}Winning solution}{8}% 
\contentsline {subsection}{\numberline {5.3}Locks}{8}% 
\contentsline {subsubsection}{\numberline {5.3.1}Double-checked locking}{8}% 
\contentsline {subsection}{\numberline {5.4}Condition variables}{9}% 
\contentsline {subsubsection}{\numberline {5.4.1}Differences between regular OS and xk}{9}% 
\contentsline {subsubsection}{\numberline {5.4.2}Mesa v Hoare semantics}{9}% 
\contentsline {section}{\numberline {6}Implementing Synchronization}{9}% 
\contentsline {subsection}{\numberline {6.1}Lock implementation with test-and-set}{9}% 
\contentsline {subsection}{\numberline {6.2}Lock implementation with CAS}{10}% 
\contentsline {subsection}{\numberline {6.3}Spinlocks v Sleeplocks}{10}% 
\contentsline {section}{\numberline {7}Signal handling}{10}% 
\contentsline {section}{\numberline {8}CPU Scheduling}{10}% 
\contentsline {subsection}{\numberline {8.1}FIFO/FCFS}{10}% 
\contentsline {subsection}{\numberline {8.2}Shortest Job First (SJF) /Shortest Remaining Time First (SRTF)}{10}% 
\contentsline {subsection}{\numberline {8.3}Round Robin}{10}% 
\contentsline {subsection}{\numberline {8.4}Multilevel Feedback Queue (MFQ)}{11}% 
\contentsline {section}{\numberline {9}Multiprocessor scheduling}{11}% 
\contentsline {subsection}{\numberline {9.1}Per-processor Affinity Scheduling}{11}% 
\contentsline {section}{\numberline {10}File System}{11}% 
\contentsline {subsection}{\numberline {10.1}FS call layers}{11}% 
\contentsline {subsection}{\numberline {10.2}Extent management implementations}{11}% 
\contentsline {subsubsection}{\numberline {10.2.1}pre-alloc}{11}% 
\contentsline {subsubsection}{\numberline {10.2.2}array}{11}% 
\contentsline {subsubsection}{\numberline {10.2.3}Unix-style indirection}{11}% 
\contentsline {subsection}{\numberline {10.3}Transactions and Atomicity/Logging}{11}% 
\contentsline {subsection}{\numberline {10.4}Inodes}{11}% 
\contentsline {section}{\numberline {11}Persistent disk}{12}% 
\contentsline {subsection}{\numberline {11.1}RAID (Redundant Array of Independent Disks)}{12}% 
\contentsline {subsection}{\numberline {11.2}NVM \& Flash Storage}{12}% 
\contentsline {subsection}{\numberline {11.3}Disk data structures}{12}% 
\contentsline {subsubsection}{\numberline {11.3.1}Bitmap}{12}% 
\contentsline {subsubsection}{\numberline {11.3.2}Log region}{12}% 
\contentsline {subsubsection}{\numberline {11.3.3}(D)inodes}{12}% 
\contentsline {subsubsection}{\numberline {11.3.4}Superblock}{12}% 
\contentsline {subsubsection}{\numberline {11.3.5}Boot block(s)}{12}% 
\contentsline {subsection}{\numberline {11.4}Disk and cache consistency}{12}% 
\contentsline {section}{\numberline {12}Paging}{12}% 
\contentsline {subsection}{\numberline {12.1}Purpose}{12}% 
\contentsline {subsection}{\numberline {12.2}Fragmentation}{13}% 
\contentsline {section}{\numberline {13}Page Replacement}{13}% 
\contentsline {subsection}{\numberline {13.1}Random}{13}% 
\contentsline {subsection}{\numberline {13.2}FIFO}{13}% 
\contentsline {subsection}{\numberline {13.3}LRU}{13}% 
\contentsline {subsubsection}{\numberline {13.3.1}Approximating LRU}{13}% 
\contentsline {subsection}{\numberline {13.4}Multiple Page Levels}{13}% 
\contentsline {subsubsection}{\numberline {13.4.1}Paged segmentation}{13}% 
\contentsline {subsubsection}{\numberline {13.4.2}Portability}{13}% 
\contentsline {subsection}{\numberline {13.5}Demand Paging}{13}% 
\contentsline {subsubsection}{\numberline {13.5.1}Memory-mapped files}{13}% 
\contentsline {subsection}{\numberline {13.6}Address Translation and the TLB}{14}% 
\contentsline {subsubsection}{\numberline {13.6.1}Base and bounds}{14}% 
\contentsline {subsubsection}{\numberline {13.6.2}Segmentation}{14}% 
\contentsline {subsubsection}{\numberline {13.6.3}TLB}{14}% 
\contentsline {subsection}{\numberline {13.7}Page Faults}{14}% 
\contentsline {section}{\numberline {14}Virtual Memory}{14}% 
\contentsline {subsection}{\numberline {14.1}Linear vs Virtual vs Physical Address}{14}% 
\contentsline {subsection}{\numberline {14.2}Self-paging}{15}% 
\contentsline {subsection}{\numberline {14.3}Swapping}{15}% 
